# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool

__all__ = ['ShipmentInternal']

__metaclass__ = PoolMeta


class ShipmentInternal:
    __name__ = 'stock.shipment.internal'
    production = fields.Many2One('production', 'Production',
                                 ondelete='RESTRICT', select=True,
                                 states={'readonly': Bool(True)},
                                 domain=[('state', '=', 'done')])

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()

        for fname in ['moves', 'from_location', 'to_location', 'company', 'group', 'planned_date']:
            field = getattr(cls, fname, None)
            if 'production' not in field.depends:
                field.depends.append('production')
            if field.states.get('readonly'):
                field.states['readonly'] = (field.states['readonly'] | Eval('production'))
            else:
                field.states['readonly'] = Eval('production')