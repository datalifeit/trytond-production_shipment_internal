from trytond.pool import Pool
from .shipment import *
from .production import *


def register():
    Pool.register(
        Configuration,
        Production,
        ShipmentInternal,
        ProductionShipmentStart,
        ProductionShipmentData,
        ProductionShipmentConfirm,
        module='production_shipment_internal', type_='model')
    Pool.register(
        ProductionShipment,
        module='production_shipment_internal', type_='wizard')